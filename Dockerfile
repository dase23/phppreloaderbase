FROM devilbox/php-fpm-7.4:latest

RUN docker-php-ext-install opcache

ADD php.ini /usr/local/etc/php